// Imports
const express = require('express');
const app = express();

// Routes
const forms = require('./routes/form.js');
const data = require('./routes/data.js');


// Settings
app.set('view engine', 'pug');

app.use(express.static('public'));
app.use(express.urlencoded({
    extended: false
}));

// Constants
const PORT = parseInt(process.env.PORT, 10) || 3000;

app.get('/', (req, res)=>{
    res.render('index');
})

app.use('/forms', forms);
app.use('/data', data);


app.listen(PORT, ()=>{
    console.log(`Listening on port ${PORT}`);
})