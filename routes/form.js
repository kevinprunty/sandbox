const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const playDataPath = './data/playData.json';

router.get('/', (req, res) => {
    if (!fs.existsSync(playDataPath)){
        fs.writeFileSync('./data/playData.json', "[]");
    }
    const obj = JSON.parse(fs.readFileSync(playDataPath));
    res.render('formpage', {obj: obj});
})

router.post('/', (req, res) =>{
    const input = req.body.input;
    const rawData = fs.readFileSync('./data/playData.json');
    const obj = JSON.parse(rawData);
    const newObj = {"input" : input};
    obj.push(newObj);
    fs.writeFileSync('./data/playData.json', JSON.stringify(obj));
    res.render('formpage', {input: input, obj: obj});
})

router.get('/addData', (req, res)=>{
    res.render('input');
})
router.get('/viewData', (req, res)=>{
    res.render('display');
})
module.exports = router;