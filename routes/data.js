const express = require('express');
const router = express.Router();
const fs = require('fs');

const filePath = './data/todo.json';

router.get('/', (req, res)=>{
    const mode = req.query.mode;

    if (!fs.existsSync(filePath)){
        fs.writeFileSync(filePath, "[]");
    }
    


    if(mode === 'view'){
        res.render('display', {data: JSON.parse(fs.readFileSync(filePath))});
    } else if (mode === 'input'){
        res.render('input');
    } else {
        res.render('index');
    }
})

router.post('/', (req, res) => {
    const date = new Date();
    const readableDate = date.toDateString();

    const body = req.body;
    const name = body.name;
    const desc = body.description;
    const due = body.dueDate;

    const array = JSON.parse(fs.readFileSync(filePath));
    const obj = {
        name, desc, due, addedDate : readableDate
    }
    array.push(obj);

    fs.writeFileSync(filePath, JSON.stringify(array));
    res.render('input');

    

})

module.exports = router;
